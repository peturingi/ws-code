/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 
package travelgood.ws.dtu.dk;

import javax.ws.rs.core.Response;
import dk.dtu.ws.lameduck.AirportType;
import dk.dtu.ws.lameduck.BookFlightFault_Exception;
import dk.dtu.ws.lameduck.BookFlightRequest;
import dk.dtu.ws.lameduck.CancelFlightRequest;
import dk.dtu.ws.lameduck.CreditCardInfoType;
import dk.dtu.ws.lameduck.ExpirationDateType;
import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.lameduck.FlightPathType;
import dk.dtu.ws.lameduck.Flights;
import dk.dtu.ws.lameduck.FlightsQuery;
import dk.dtu.ws.niceview.BookHotelFaultType;
import dk.dtu.ws.niceview.GetHotelsRequest;
import dk.dtu.ws.niceview.BookHotelRequst;
import dk.dtu.ws.niceview.CancelHotelRequest;
import dk.dtu.ws.niceview.HotelsType;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import travelgood.ws.entities.Itinerary;
/**
 * REST Web Service
 *
 * @author jcfinnerup
 */
/*
@Path("/travelgood")
public class TravelGood {
    private static List<Itinerary> Itineries = new ArrayList<Itinerary>();
    private static final Map<String, String> airportToCodes;
    static
    {
        airportToCodes = new HashMap<String, String>();
        airportToCodes.put("Copenhagen Airport", "CPH");
        airportToCodes.put("Athens International Airport", "ATH");
    }
    
    /**
     * saves a Itinerary. If the Itinerary is a new on create id and save to 
     * list and return is, if the Itinerary has a id (already exists in list) 
     * Then it will save it
     * @param item Itinerary
     * @return  Itinerary object or null if something went wrong
     */
/*
    private Itinerary saveItinerary(Itinerary item)
    {
        if(item.getId() == 0)
        {
            if(Itineries.isEmpty())
            {
                item.setId(1);
            }
            else
            {
                Itinerary lastItinerary = Itineries.get(Itineries.size()-1);
                item.setId(lastItinerary.getId()+1);
            }   
            Itineries.add(item);
            return item;
        }
        else
        {
            for(int n = 0; n < Itineries.size(); n++)
            {
                if(item.getId() == Itineries.get(n).getId())
                {
                    Itineries.set(n,item);
                    return item;
                }
            }
            return null;
        }
    }
    
    /**
     * Gets a Itinerary from the list
     * @param id id of Itinerary
     * @return Itinerary or null if not found.
     *//*
    private Itinerary findItinerary(String id)
    {
        int bookingNum = Integer.parseInt(id);
        for(int n = 0; n < Itineries.size(); n++)
        {
            if(bookingNum == Itineries.get(n).getId())
            {
                return Itineries.get(n);
            }
        }
        return null;
    }
    
    /**
     * Removes a Itinerary from the list
     * @param id id of Itinerary
     *//*
    private void deleteItinerary(int id)
    {
        for(int n = 0; n < Itineries.size(); n++)
        {
            if(id == Itineries.get(n).getId())
            {
                if (Itineries.get(n).getStatus().equals("Confirmed")) {
                    for (String bookedHotelId : Itineries.get(n).getHotels()) {
                        CancelHotelRequest cancelRequest = new CancelHotelRequest();
                        cancelRequest.setBookingNumber(bookedHotelId);
                        cancelHotel(cancelRequest);
                    }
                }
                Itineries.get(n).setStatus("Cancelled");
            }
        }
    }
    
    /////////////////
    /** Itinerary **/
    /////////////////
    
  /*  
    @POST
    @Path("/itinerary/{id}/book")
    public boolean bookItinerary(@PathParam("id") String id, @QueryParam("cardNum") String cardNum, @QueryParam("name") String name, @QueryParam("year") int year, @QueryParam("month") int month) throws BookFlightFault_Exception, BookHotelFaultType{
        Itinerary it = findItinerary(id);
        ArrayList<String> flights = it.getFlights();
        ArrayList<String> hotels = it.getHotels();
        
        ExpirationDateType exp = new ExpirationDateType();
        exp.setMonth(month);
        exp.setYear(year);
        
        dk.dtu.ws.niceview.ExpirationDateType ed = new dk.dtu.ws.niceview.ExpirationDateType();   
        ed.setMonth(month);
        ed.setYear(year);
        
        CreditCardInfoType creditCard = new CreditCardInfoType();
        creditCard.setName(name);
        creditCard.setNumber(cardNum);
        creditCard.setExpirationDate(exp);
        
        dk.dtu.ws.niceview.CreditCardInfoType ccit = new dk.dtu.ws.niceview.CreditCardInfoType();
        ccit.setExpirationDate(ed);
        ccit.setName(name);
        ccit.setNumber(cardNum);
        
        ArrayList<String> bookedFlights = new ArrayList<String>();
        for(String flightId : flights){
            BookFlightRequest request = new BookFlightRequest();
            request.setCreditCard(creditCard);
            request.setBookingNumber(flightId);
            boolean success = bookFlight(request);
            if(!success)
                // The user is not able to get their money back for any flights
                // that they may have already been charged for. This is due to
                // TravelGood not having any cost information about the flight,
                // so we can not create a CancelFlightRequest object.
                
//                for (String bookedFlightId : bookedFlights) {
//                    CancelFlightRequest flightRequest = new CancelFlightRequest();
//                    flightRequest.getPrice();
//                }
                return success;
       
        }
        ArrayList<String> bookedHotels = new ArrayList<String>();
        for(String hotelId : hotels){
             BookHotelRequst request = new BookHotelRequst();
             request.setBookingNumber(hotelId);
             request.setCreditCardInfo(ccit);
             boolean success = bookHotel(request);
                if(!success) {
                    for (String bookedHotelId : bookedHotels) {
                        CancelHotelRequest cancelRequest = new CancelHotelRequest();
                        cancelRequest.setBookingNumber(bookedHotelId);
                        cancelHotel(cancelRequest);
                    }
                    return success;
                }
             bookedHotels.add(hotelId);
        }
        it.setStatus("Confirmed");
        saveItinerary(it);
        return true;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_XML)
    @Path("/itinerary")
    public Itinerary createItinerary() throws DatatypeConfigurationException{
        Itinerary it = new Itinerary();
        it.setStatus("Unconfirmed");
        it = saveItinerary(it);
        return it;
    }

    @GET
    @Path("/itinerary/{id}")
    public Itinerary getItinerary(@PathParam("id") String id){
        return findItinerary(id);
    }
    
    @DELETE
    @Path("/itinerary/{id}")
    public void cancelItinerary(@PathParam("id") String id){
        int bookingNum = Integer.parseInt(id);
        deleteItinerary(bookingNum);
    }

    /////////////////
    /**  Flights  **/
    /////////////////
    /*
    @POST
    @Path("/itinerary/{id}/flight")
    public Response addFlight(@PathParam("id") String id, @QueryParam("bookingNum") String bookingNum) {
        Itinerary it = findItinerary(id);
        if (it != null) {
            if(it.getStatus().equals("Confirmed")) {
                return Response.status(Response.Status.FORBIDDEN).build();
            }
            //it.addFlight(bookingNum);
            it.getFlights().add(bookingNum);
            it = saveItinerary(it);
            return Response.ok(it).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Path("/flights")
    public Flights getFlights(@QueryParam("milliseconds") String milliseconds, @QueryParam("fromName") String fromName, @QueryParam("toName") String toName) throws DatatypeConfigurationException {
        long dateLong = Long.parseLong(milliseconds);
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(dateLong);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        return getFlightsList(xmlCal, fromName, toName);
    }
    
    private static Flights getLameFlights(dk.dtu.ws.lameduck.FlightsQuery getFlightsQuery) {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        return port.getFlights(getFlightsQuery);
    }
    
    public Flights getFlightsList(XMLGregorianCalendar date, String fromName, String toName){
        String fromCode = airportToCodes.get(fromName);
        String toCode = airportToCodes.get(toName);
        AirportType fromAirport = new AirportType();
        fromAirport.setName(fromName);
        fromAirport.setCode(fromCode);
        
        AirportType toAirport = new AirportType();
        toAirport.setName(toName);
        toAirport.setCode(toCode);
        
        FlightPathType path = new FlightPathType();
        path.setFrom(fromAirport);
        path.setTo(toAirport);
        
        FlightsQuery newQueue = new FlightsQuery();
        newQueue.setDate(date);
        newQueue.setPath(path);
        Flights listOfFlights = getLameFlights(newQueue);
        
        return listOfFlights;
    }
    
    /////////////////
    /**  Hotels   **/
    /////////////////
    /*
    @POST
    @Path("/itinerary/{id}/hotel")
    public Response addHotel(@PathParam("id") String id, @QueryParam("hotelId") String hotelId){
        Itinerary it = findItinerary(id);
        if (it != null) {
            if(it.getStatus().equals("Confirmed")) {
                return Response.status(Response.Status.FORBIDDEN).build();
            }
            it.getHotels().add(hotelId);
            it = saveItinerary(it);
            return Response.ok(it).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Path("/hotels")
    public HotelsType getHotels(@QueryParam("city") String city, @QueryParam("arrivalTime") String arrivalTime, @QueryParam("departureTime") String departureTime) throws DatatypeConfigurationException {
        long arrivalLong = Long.parseLong(arrivalTime);
        GregorianCalendar arrivalCal = new GregorianCalendar();
        Date arrivalDate = new Date(arrivalLong);
        arrivalCal.setTime(arrivalDate);
        XMLGregorianCalendar arrivalXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(arrivalCal);
        
        long departureLong = Long.parseLong(departureTime);
        GregorianCalendar departureCal = new GregorianCalendar();
        Date departureDate = new Date(departureLong);
        departureCal.setTime(departureDate);
        XMLGregorianCalendar departureXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(departureCal);
        
        GetHotelsRequest request = new GetHotelsRequest();
        request.setCity(city);
        request.setArrival(arrivalXMLCal);
        request.setDeparture(departureXMLCal);
        return getNiceHotels(request);
    }

    private static HotelsType getNiceHotels(dk.dtu.ws.niceview.GetHotelsRequest request) {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        return port.getHotels(request);
    }

    private static boolean bookFlight(dk.dtu.ws.lameduck.BookFlightRequest bookFlightRequest) throws BookFlightFault_Exception {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        return port.bookFlight(bookFlightRequest);
    }

    private static boolean bookHotel(dk.dtu.ws.niceview.BookHotelRequst request) throws BookHotelFaultType {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        return port.bookHotel(request);
    }

    private static void cancelHotel(dk.dtu.ws.niceview.CancelHotelRequest request) {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        port.cancelHotel(request);
    }

}
*/
