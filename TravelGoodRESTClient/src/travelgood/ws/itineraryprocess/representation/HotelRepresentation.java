/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package travelgood.ws.itineraryprocess.representation;

import dk.dtu.ws.niceview.HotelsType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jcfinnerup
 */
@XmlRootElement()
public class HotelRepresentation extends Representation{
    
    private HotelsType hotels;

    public HotelsType getHotels() {
        return this.hotels;
    }

    public void setHotels(HotelsType hotels) {
        this.hotels = hotels;
    }
    
}
