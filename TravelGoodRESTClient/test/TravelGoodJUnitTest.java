/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.WebResource;
import dk.dtu.ws.lameduck.AirportType;
import dk.dtu.ws.lameduck.CreditCardInfoType;
import dk.dtu.ws.lameduck.ExpirationDateType;
import dk.dtu.ws.lameduck.FlightInfo;
import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.lameduck.FlightPathType;
import dk.dtu.ws.lameduck.Flights;
import dk.dtu.ws.lameduck.FlightsQuery;
import dk.dtu.ws.niceview.HotelQuery;
import dk.dtu.ws.niceview.HotelInfo;
import dk.dtu.ws.niceview.HotelType;
import dk.dtu.ws.niceview.HotelsType;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelgood.ws.entities.CreditCardWrapper;
import travelgood.ws.itineraryprocess.representation.FlightRepresentation;
import travelgood.ws.itineraryprocess.representation.HotelRepresentation;
import travelgood.ws.itineraryprocess.representation.ItineraryRepresentation;
import travelgood.ws.itineraryprocess.representation.StatusRepresentation;

/**
 *
 * @author jcfinnerup
 */
public class TravelGoodJUnitTest {
    
    static final String MEDIATYPE_ITINERARYPROCESS = "application/itineraryprocess+xml"; 
    Client client;
    String baseURI;
    WebResource itineraries;
    
    private static final Map<String, String> airportToCodes;
       static
       {
           airportToCodes = new HashMap<String, String>();
           airportToCodes.put("Copenhagen Airport", "CPH");
           airportToCodes.put("Athens International Airport", "ATH");
       }
    
    public TravelGoodJUnitTest() {
        client = Client.create();
        baseURI = "http://localhost:8080/TravelGoodREST";
        itineraries = client.resource(baseURI).path("itineraries");
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
//        client.resource(itineraries.getURI().toString() + "/reset").put(); 
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void createItinerary() {
        String cid = "1";
        String iid = "henning";
        
        StatusRepresentation result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
                accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);

        assertEquals("created", result.getStatus());
        
        cid = "2";
        iid = "sundkvist";
        
        result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
                accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);

        assertEquals("created", result.getStatus());
        
        cid = "3";
        iid = "weinstein";
        
        result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
                accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);

        assertEquals("created", result.getStatus());
    }
    
    @Test
    public void cancelItinerary() {
        CreditCardWrapper creditCards = new CreditCardWrapper();
        creditCards.setLameCard(getLameCard());
        creditCards.setNiceCard(getNiceCard());
        String cid = "1";
        String iid = "henning";
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/cancel";
        StatusRepresentation result = client.resource(path).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, creditCards);
        
        assertEquals("cancelled", result.getStatus());
    }
    
    @Test
    public void getFlights() throws DatatypeConfigurationException {
        long dateLong = 1416246128000L;
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(dateLong);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        FlightsQuery query = createFlightsQuery(xmlCal, "Copenhagen Airport", "Athens International Airport");
        FlightRepresentation result = getFlightsCall(query);
        
        assertEquals(1, result.getFlights().getFlightInfo().size());
        assertTrue(result.getFlights().getFlightInfo().get(0).getPrice().getAmount() > 0);
        assertTrue(result.getFlights().getFlightInfo().get(0).getFlight().getPath().getFrom().getName().equals("Copenhagen Airport"));
    }
    
    @Test
    public void getHotels() throws DatatypeConfigurationException {
        long arrivalLong = 1418674616000L;
        GregorianCalendar arrivalCal = new GregorianCalendar();
        Date arrivalDate = new Date(arrivalLong);
        arrivalCal.setTime(arrivalDate);
        XMLGregorianCalendar arrivalXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(arrivalCal);
        
        long departureLong = 1419279416000L;
        GregorianCalendar departureCal = new GregorianCalendar();
        Date departureDate = new Date(departureLong);
        departureCal.setTime(departureDate);
        XMLGregorianCalendar departureXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(departureCal);
        
        HotelQuery request = new HotelQuery();
        request.setCity("Copenhagen");
        request.setArrival(arrivalXMLCal);
        request.setDeparture(departureXMLCal);
        
        HotelRepresentation result = getHotelsCall(request);
        assertTrue(result.getHotels().getHotel().size() > 0);
        assertTrue(result.getHotels().getHotel().get(0).getPrice() > 0);   
    }
    
    @Test
    public void addFlight() throws DatatypeConfigurationException {
        long dateLong = 1416246128000L;
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(dateLong);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        
        FlightsQuery query = createFlightsQuery(xmlCal, "Copenhagen Airport", "Athens International Airport");
        FlightRepresentation flightResult = getFlightsCall(query);
        FlightInfoType flight = flightResult.getFlights().getFlightInfo().get(0);
        FlightInfo flightInfo = new FlightInfo();
        flightInfo.setFlightInfo(flight);
        
        String cid = "2";
        String iid = "sundkvist";
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/addflight";
        StatusRepresentation result = client.resource(path).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, flightInfo);
        assertTrue(result.getStatus().equals("flight added to itinerary"));
    }
    
    @Test
    public void addHotel() throws DatatypeConfigurationException {
        long arrivalLong = 1418674616000L;
        GregorianCalendar arrivalCal = new GregorianCalendar();
        Date arrivalDate = new Date(arrivalLong);
        arrivalCal.setTime(arrivalDate);
        XMLGregorianCalendar arrivalXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(arrivalCal);
        
        long departureLong = 1419279416000L;
        GregorianCalendar departureCal = new GregorianCalendar();
        Date departureDate = new Date(departureLong);
        departureCal.setTime(departureDate);
        XMLGregorianCalendar departureXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(departureCal);
        
        HotelQuery request = new HotelQuery();
        request.setCity("Copenhagen");
        request.setArrival(arrivalXMLCal);
        request.setDeparture(departureXMLCal);
        
        HotelRepresentation result = getHotelsCall(request);
        HotelType hotel = result.getHotels().getHotel().get(0);
        HotelInfo hotelInfo = new HotelInfo();
        hotelInfo.setHotel(hotel);
        String cid = "2";
        String iid = "sundkvist";
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/addhotel";
        
        StatusRepresentation status = client.resource(path).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, hotelInfo);
        assertTrue(status.getStatus().equals("hotel added to itinerary"));
    }
    
    @Test
    public void bookItinerary() {
        CreditCardWrapper cards = new CreditCardWrapper();
        cards.setLameCard(getLameCard());
        cards.setNiceCard(getNiceCard());
        String cid = "2";
        String iid = "sundkvist";
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/book";
        
        StatusRepresentation status = client.resource(path).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, cards);
        assertEquals("booked", status.getStatus());
    }
    
    private FlightsQuery createFlightsQuery(XMLGregorianCalendar date, String fromName, String toName){
        String fromCode = airportToCodes.get(fromName);
        String toCode = airportToCodes.get(toName);
        AirportType fromAirport = new AirportType();
        fromAirport.setName(fromName);
        fromAirport.setCode(fromCode);
        
        AirportType toAirport = new AirportType();
        toAirport.setName(toName);
        toAirport.setCode(toCode);
        
        FlightPathType path = new FlightPathType();
        path.setFrom(fromAirport);
        path.setTo(toAirport);
        
        FlightsQuery newQuery = new FlightsQuery();
        newQuery.setDate(date);
        newQuery.setPath(path);
        
        return newQuery;
    }
    private FlightRepresentation getFlightsCall(FlightsQuery query) throws DatatypeConfigurationException {
        String cid = "2";
        String iid = "sundkvist";
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/flights";
        FlightRepresentation flightResult = client.resource(path).type(MEDIATYPE_ITINERARYPROCESS)
                .put(FlightRepresentation.class, query);
        return flightResult;
    }
    private HotelRepresentation getHotelsCall(HotelQuery request) {
        String cid = "3";
        String iid = "weinstein";
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/hotels";
        HotelRepresentation result = client.resource(path).type(MEDIATYPE_ITINERARYPROCESS).
                put(HotelRepresentation.class, request);
        return result;
    }
    
      /** Get Lame Duck Credit Card **/
    private CreditCardInfoType getLameCard(){
        dk.dtu.ws.lameduck.CreditCardInfoType lameCard = new CreditCardInfoType();
        dk.dtu.ws.lameduck.ExpirationDateType exp = new ExpirationDateType();
        exp.setMonth(5);
        exp.setYear(9);
        lameCard.setExpirationDate(exp);
        lameCard.setName("Anne Strandberg");
        lameCard.setNumber("5040881");
        return lameCard;
    }
    
    /** Get Nice View Credit Card **/
    private dk.dtu.ws.niceview.CreditCardInfoType getNiceCard(){
        dk.dtu.ws.niceview.CreditCardInfoType niceCard = new dk.dtu.ws.niceview.CreditCardInfoType();
        dk.dtu.ws.niceview.ExpirationDateType exp = new dk.dtu.ws.niceview.ExpirationDateType();
        exp.setMonth(5);
        exp.setYear(9);
        niceCard.setExpirationDate(exp);
        niceCard.setName("Anne Strandberg");
        niceCard.setNumber("5040881");
        return niceCard;
    }
}
