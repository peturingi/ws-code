
package types.lameduck.ws.dtu.dk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlightPathType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FlightPathType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="from" type="{dk.dtu.ws.lameduck.types}AirportType"/>
 *         &lt;element name="to" type="{dk.dtu.ws.lameduck.types}AirportType"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightPathType", propOrder = {

    })
public class FlightPathType {

    @XmlElement(required = true)
    protected AirportType from;
    @XmlElement(required = true)
    protected AirportType to;

    /**
     * Gets the value of the from property.
     *
     * @return
     *     possible object is
     *     {@link AirportType }
     *
     */
    public AirportType getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     *
     * @param value
     *     allowed object is
     *     {@link AirportType }
     *
     */
    public void setFrom(AirportType value) {
        this.from = value;
    }

    /**
     * Gets the value of the to property.
     *
     * @return
     *     possible object is
     *     {@link AirportType }
     *
     */
    public AirportType getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     *
     * @param value
     *     allowed object is
     *     {@link AirportType }
     *
     */
    public void setTo(AirportType value) {
        this.to = value;
    }

}
