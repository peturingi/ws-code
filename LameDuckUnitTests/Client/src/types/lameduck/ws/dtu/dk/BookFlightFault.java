
package types.lameduck.ws.dtu.dk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import dk.dtu.imm.fastmoney.types.CreditCardFaultType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rootFault" type="{urn://types.fastmoney.imm.dtu.dk}CreditCardFaultType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "message", "rootFault" })
@XmlRootElement(name = "bookFlightFault")
public class BookFlightFault {

    protected String message;
    protected CreditCardFaultType rootFault;

    /**
     * Gets the value of the message property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the rootFault property.
     *
     * @return
     *     possible object is
     *     {@link CreditCardFaultType }
     *
     */
    public CreditCardFaultType getRootFault() {
        return rootFault;
    }

    /**
     * Sets the value of the rootFault property.
     *
     * @param value
     *     allowed object is
     *     {@link CreditCardFaultType }
     *
     */
    public void setRootFault(CreditCardFaultType value) {
        this.rootFault = value;
    }

}
