package dk.dtu.imm.tests;

import java.util.Date;
import java.util.GregorianCalendar;

import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import static org.junit.Assert.*;
import org.junit.Test;

import lameduck.ws.dtu.dk.LameDuckPortType;
import lameduck.ws.dtu.dk.LameDuckService;

import types.lameduck.ws.dtu.dk.AirportType;
import types.lameduck.ws.dtu.dk.CarrierType;
import types.lameduck.ws.dtu.dk.FlightInfoType;
import types.lameduck.ws.dtu.dk.FlightPathType;
import types.lameduck.ws.dtu.dk.FlightType;
import types.lameduck.ws.dtu.dk.Flights;
import types.lameduck.ws.dtu.dk.FlightsQuery;
import types.lameduck.ws.dtu.dk.MoneyType;

public class LameDuckTest {
    public LameDuckTest() {
    }

    @Test
    public void oneFlightAtFirstDate() {
        LameDuckService lameDuckService = new LameDuckService();
        LameDuckPortType lameDuckPortType = lameDuckService.getLameDuckPortTypeBindingPort();
        // Add your code to call the desired methods.

        FlightsQuery query =
            createQuery("CPH", "Copenhagen Airport", "ATH", "Athens International Airport", 1416124162000L+100000000L);
        Flights flights = lameDuckPortType.getFlights(query);
        List<FlightInfoType> fiList = flights.getFlightInfo();
        assertTrue("Expected one flight", fiList.size() == 1);
    }
    

    @Test
    public void noFlightInBeginningOfTime() {
        LameDuckService lameDuckService = new LameDuckService();
        LameDuckPortType lameDuckPortType = lameDuckService.getLameDuckPortTypeBindingPort();
        // Add your code to call the desired methods.

        FlightsQuery query =
            createQuery("CPH", "Copenhagen Airport", "ATH", "Athens International Airport", 0L);
        Flights flights = lameDuckPortType.getFlights(query);
        List<FlightInfoType> fiList = flights.getFlightInfo();
        assertTrue("Expected no flight", fiList.size() == 0);
    }

    private FlightsQuery createQuery(String departureAirportCode, String departureAirportName,
                                     String destinationAirportcode, String destinationAirportName, long departureDate) {
        // Create airports.
        AirportType from = new AirportType();
        from.setCode(departureAirportCode);
        from.setName(departureAirportName);
        AirportType to = new AirportType();
        to.setCode(destinationAirportcode);
        to.setName(destinationAirportName);
        //
        FlightPathType fp = new FlightPathType();
        fp.setFrom(from);
        fp.setTo(to);
        //
        FlightsQuery fq = new FlightsQuery();
        fq.setDate(dateToXMLCal(departureDate));
        fq.setPath(fp);
        return fq;
    }

    private static XMLGregorianCalendar dateToXMLCal(long time) {
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(time);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return xmlCal;
    }
}
