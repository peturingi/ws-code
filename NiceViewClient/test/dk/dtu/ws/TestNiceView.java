/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.rpc.soap.SOAPFaultException;
import niceview.ws.dtu.dk.BookHotelFaultType;
import org.junit.Test;
import static org.junit.Assert.*;
import types.niceview.ws.dtu.dk.HotelsType;

/**
 *
 * @author obeid
 */
public class TestNiceView {
    
    @Test
    public void testGetHotels(){
        DatatypeFactory df = null;
        try { df = DatatypeFactory.newInstance(); } 
        catch (DatatypeConfigurationException ex) { /* todo: handle it? */ }
        
        types.niceview.ws.dtu.dk.HotelQuery hotelQuery = new types.niceview.ws.dtu.dk.HotelQuery();
        hotelQuery.setArrival(df.newXMLGregorianCalendar("2014-12-15"));
        hotelQuery.setDeparture(df.newXMLGregorianCalendar("2014-12-22"));
        hotelQuery.setCity("Aalborg");
        
        HotelsType hotels = getHotels(hotelQuery);
        assertEquals(2, hotels.getHotel().size());
    }
    
    @Test
    public void testBookHotel() throws BookHotelFaultType{
        assertTrue(bookHotel(requestCorrectBookHotelInfo("2")));
    }
    
    @Test
    public void testCancelHotel() throws BookHotelFaultType{
        bookHotel(requestCorrectBookHotelInfo("3"));
        
        types.niceview.ws.dtu.dk.CancelHotelRequest cancelRequest = new types.niceview.ws.dtu.dk.CancelHotelRequest();
        cancelRequest.setBookingNumber("3");
        
        try{
            cancelHotel(cancelRequest);
        }
        catch(SOAPFaultException e){
            fail("Cancel Hotel threw exception" + e.getMessage());
        }
        
        assertTrue(true);
    }
    
    @Test(expected=BookHotelFaultType.class)
    public void testWrongBookHotel() throws BookHotelFaultType {
        bookHotel(requestWrongBookHotelInfo("3"));
    }
    
//    @Test(expected=SOAPFaultException.class)
//    public void testWrongCancelHotel() throws Exception {
//        types.niceview.ws.dtu.dk.CancelHotelRequest cancelRequest = new types.niceview.ws.dtu.dk.CancelHotelRequest();
//        cancelRequest.setBookingNumber("0");
//        cancelHotel(cancelRequest);
//    }

    
    public niceview.ws.dtu.dk.BookHotelRequest requestCorrectBookHotelInfo(String bn){
       niceview.ws.dtu.dk.BookHotelRequest request = new niceview.ws.dtu.dk.BookHotelRequest();
        
        dk.dtu.imm.fastmoney.types.ExpirationDateType ex = new dk.dtu.imm.fastmoney.types.ExpirationDateType();
        ex.setMonth(5);
        ex.setYear(9);
        
        dk.dtu.imm.fastmoney.types.CreditCardInfoType creditcard = new dk.dtu.imm.fastmoney.types.CreditCardInfoType();
        creditcard.setExpirationDate(ex);
        creditcard.setName("Anne Strandberg");
        creditcard.setNumber("50408816");
        
        request.setBookingNumber(bn);
        request.setCreditCardInfo(creditcard);
        
        return request;
    }
    
    public niceview.ws.dtu.dk.BookHotelRequest requestWrongBookHotelInfo(String bn){
        niceview.ws.dtu.dk.BookHotelRequest request = new niceview.ws.dtu.dk.BookHotelRequest();
        
        dk.dtu.imm.fastmoney.types.ExpirationDateType ex = new dk.dtu.imm.fastmoney.types.ExpirationDateType();
        ex.setMonth(5);
        ex.setYear(9);
        
        dk.dtu.imm.fastmoney.types.CreditCardInfoType creditcard = new dk.dtu.imm.fastmoney.types.CreditCardInfoType();
        creditcard.setExpirationDate(ex);
        creditcard.setName("Jens Jensen");
        creditcard.setNumber("5040881");
        
        request.setBookingNumber(bn);
        request.setCreditCardInfo(creditcard);
        
        return request;
    }
    
    // Web Service References
    
    private static boolean bookHotel(niceview.ws.dtu.dk.BookHotelRequest request) throws BookHotelFaultType {
        niceview.ws.dtu.dk.NiceViewWSDLService service = new niceview.ws.dtu.dk.NiceViewWSDLService();
        niceview.ws.dtu.dk.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        return port.bookHotel(request);
    }

    private static void cancelHotel(types.niceview.ws.dtu.dk.CancelHotelRequest request) {
        niceview.ws.dtu.dk.NiceViewWSDLService service = new niceview.ws.dtu.dk.NiceViewWSDLService();
        niceview.ws.dtu.dk.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        port.cancelHotel(request);
    }

    private static HotelsType getHotels(types.niceview.ws.dtu.dk.HotelQuery request) {
        niceview.ws.dtu.dk.NiceViewWSDLService service = new niceview.ws.dtu.dk.NiceViewWSDLService();
        niceview.ws.dtu.dk.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        return port.getHotels(request);
    }
}