package dk.dtu;

import java.util.Currency;

/**
 * TODO 
 * Represents a monetary value.
 * @see Book: Patterns of Enterprise Application Architecture, page 488, by Martin Fowler.
 */
public class Money {
    
    private int amount;
    private String currency; // TODO refactor to Enum.
    
    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }
    
    public int getamount() {
        return this.amount;
    }
    
    public String getCurrency() {
        return this.currency;
    }
    
}
