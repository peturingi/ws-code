package dk.dtu.airline;

/**
 * Airline Carrier.
 * 
 * @author P�tur Ingi Egilsson
 */
public class Carrier {
    private String name;
    public Carrier(String name) {
        // TODO confirm name is not null.
        super();
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    
    public String toString() {
        return this.name;
    }
}
