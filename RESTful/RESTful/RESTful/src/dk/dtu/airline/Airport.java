package dk.dtu.airline;

/**
 * Airport.
 * 
 * @author P�tur Ingi Egilsson
 */
public class Airport {
    
    private String name;
    private String ICAO; // Airport Code (UUID).
    
    /**
     * Creates a new airport.
     * @param name Name of the airport.
     * @param code A unique ICAO code. Can be found for existing airports at http://www.avcodes.co.uk/aptcodesearch.asp.
     */
    public Airport(String name, String code) {
        // TODO confirm name and code are not null.
        super();
        this.name = name;
        this.ICAO = code;
    }
    
    public String getName() {
        return this.name;
    }
    
    /**
     * Returns the airports avaiation code.
     * @return ICAO
     */
    public String getICAO() {
        return this.ICAO;
    }
    
    public String toString() {
        return name + " (" + ICAO + ")";
    }
    
}
