package dk.dtu.airline;

/**
 * The flight path between two locations.
 * 
 * @Author P�tur Ingi Egilsson
 */
public class FlightPath {
    
    final Airport departure;
    final Airport destination;
    
    public FlightPath(final Airport departure, final Airport destination) {
        // TODO confirm no parameter is null.
        super();
        this.departure = departure;
        this.destination = destination;
    }

    public Airport getDeparture() {
        return departure;
    }

    public Airport getDestination() {
        return destination;
    }
    
    public String toString() {
        return "Departure airport: " + departure.toString()
            + "Destination airport: " + destination.toString();
    }
}
