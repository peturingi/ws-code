package dk.dtu.airline;

import dk.dtu.Money;

import java.util.Date;

import static org.junit.Assert.*;
import org.junit.Test;

/** Tests FlightInfo.
 * 
 * @author P�tur Ingi Egilsson
 */
public class FlightInfoTest {
    
    private final String BOOKING_NUMBER = "ABCD1234";
    private final int FLIGHT_PRICE = 99;
    private final String CARRIER_NAME = "Joakims Carrier";
    
    private final AirlineReservationService service = new AirlineReservationService("Andres And");
    private final Carrier carrier = new Carrier(CARRIER_NAME);
    private final Money price = new Money(FLIGHT_PRICE, "DKK");
    private final Airport departureLocation = new Airport("Keflavik", "KEF");
    private final Airport destinationLocation = new Airport("Copenhagen", "CPH");
    private final Flight flight = new Flight(carrier, departureLocation, destinationLocation, new Date(), new Date());
    private final FlightInfo flightInfo = new FlightInfo("ABCD1234", service, flight, price);

    
    public FlightInfoTest() {
    }

    /**
     * @see FlightInfo#getBookingNumber()
     */
    @Test
    public void testGetBookingNumber() {
        assertNotNull("Error in test.", BOOKING_NUMBER);
        assertEquals("Booking number did not match.", BOOKING_NUMBER, flightInfo.getBookingNumber());
    }

    /**
     * @see FlightInfo#getReservedWith()
     */
    @Test
    public void testGetReservedWith() {
        assertNotNull("Error in test.", service);
        assertEquals("Service did not match.", service, flightInfo.getReservedWith());
    }

    /**
     * @see FlightInfo#getFlight()
     */
    @Test
    public void testGetFlight() {
        assertNotNull("Error in test.", flight);
        assertEquals("Flight did not match.", flight, flightInfo.getFlight());
        assertEquals("Wrong departure location.", this.departureLocation, flightInfo.getFlight().getFlightPath().getDeparture());
    }

    /**
     * @see FlightInfo#getPrice()
     */
    @Test
    public void testGetPrice() {
        assertNotNull("Error in test.", price);
        assertEquals("Price did not match.", price, flightInfo.getPrice());
        assertEquals("Price did not match.", FLIGHT_PRICE, flightInfo.getPrice().getamount());
    }
}
