package dk.dtu.airline;


/**
 * Service partner who can reserve seats in an airplane on behalf of a customer.
 * 
 * @Author P�tur Ingi Egilsson
 */
public class AirlineReservationService {
    
    final String name;
    
    public AirlineReservationService(String name) {
        super();
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String toString() {
        return this.name;
    }
}
