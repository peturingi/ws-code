/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package travelgood.ws.itineraryprocess.resource;

import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.niceview.HotelInfo;
import dk.dtu.ws.niceview.HotelType;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import travelgood.ws.entities.Itinerary;
import travelgood.ws.itineraryprocess.representation.StatusRepresentation;

/**
 *
 * @author jcfinnerup
 */
@Path("itineraries/{cid}/{iid}/addhotel")
public class ItineratyAddHotelResource {
 
    @PUT
    @Consumes(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    @Produces(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    public Response addHotel(@PathParam("cid") String cid, @PathParam("iid") String iid, HotelInfo hotel) {
        
        if(hotel == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        
        String key = ItineraryResource.createKey(cid, iid);
        Itinerary itinerary = ItineraryResource.itineraries.get(key);
        
        if(itinerary == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if(!itinerary.getStatus().equals(ItineraryResource.STATUS_OPEN)) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
                
        
        itinerary.updateTimeOfFirst(hotel.getHotel().getArrival().getMillisecond());
        itinerary.getHotels().add(hotel.getHotel());
        ItineraryResource.itineraries.put(key, itinerary);
        
        StatusRepresentation response = new StatusRepresentation();
        response.setStatus(ItineraryResource.STATUS_HOTEL_ADDED);
        ItineraryResource.addSelfLink(cid,iid,response);
        ItineraryResource.addHotelsLink(cid,iid,response);
        return Response.ok(response).build(); 
    }
}



