/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package travelgood.ws.itineraryprocess.resource;

import java.util.HashMap;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import travelgood.ws.entities.Itinerary;
/**
 *
 * @author jcfinnerup
 */
@Path("itineraries/reset")
public class ItineraryResetResource {

    @PUT
    public Response reset(){
        ItineraryResource.itineraries = new HashMap<String, Itinerary>();
        return Response.ok().build();
    }
    
}
