package travelgood.ws.itineraryprocess.resource;

import dk.dtu.ws.niceview.HotelsType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import travelgood.ws.itineraryprocess.representation.HotelRepresentation;
import dk.dtu.ws.niceview.HotelQuery;
import dk.dtu.ws.niceview.HotelType;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;

@Path("itineraries/{cid}/{iid}/hotels")
public class ItineraryHotelsResource {
    @PUT
    @Consumes(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    @Produces(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    public Response getHotel(@PathParam("cid") String cid, @PathParam("iid") String iid, HotelQuery hotelQuery) { 
        
        if(hotelQuery == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        
        HotelsType result = getNiceHotels(hotelQuery);
        
        if(result.getHotel().isEmpty()) {
            return Response.status(Response.Status.NO_CONTENT).build(); 
        }
        
        HotelRepresentation response = new HotelRepresentation(); 
        response.setHotels(result);
        ItineraryResource.addSelfLink(cid,iid,response);
        ItineraryResource.addAddHotelLink(cid,iid,response);
        return Response.ok(response).build();
    }  

    private static HotelsType getNiceHotels(HotelQuery request) {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        return port.getHotels(request);
    }
}
