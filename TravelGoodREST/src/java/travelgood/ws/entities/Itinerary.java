/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package travelgood.ws.entities;

import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.niceview.HotelType;
import dk.dtu.ws.niceview.HotelsType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Jens-Christian Finnerup
 * @author Josh Weinstein
 * @author Jim Sundkvist
 */
@XmlRootElement
public class Itinerary implements Serializable{
    
    /** Variables **/
    private final long serialVersionUID = 1L;
    private String id;
    private String status;    
    private ArrayList<FlightInfoType> flights = new ArrayList<FlightInfoType>();
    private ArrayList<HotelType> hotels = new ArrayList<HotelType>();
    private int timeOfFirst; // Needs to be updated with every addition of flights (w. condition)
    
    /** Constructor - Unused **/
    public Itinerary(){
       // No need yet
    }
    
    /** Set ID **/
    public void setId(String id){
        this.id = id;
    }
    
    /** Get ID **/
    public String getId(){
        return this.id;
    }
    
    /** Set Status **/
    public void setStatus(String booked){
        this.status = booked;
    }
    
    /** Get Status **/
    public String getStatus(){
        return this.status;
    }
    
    /** Set Flights **/
    public void setFlights(ArrayList<FlightInfoType> flightList) {
        this.flights = flightList;
    }
    
    /** Add Flight **/
    public void addFlight(FlightInfoType flightNum) {
        this.flights.add(flightNum);
    }
    
    /** Get Flights **/
    public ArrayList<FlightInfoType> getFlights() {
        return this.flights;
    }
    
    /** Add Hotel **/
    public void addHotel(HotelType hotelNum){
        this.hotels.add(hotelNum);
    }
    
    /** Set Hotels **/
    public void setHotels(ArrayList<HotelType> hotelList){
        this.hotels = hotelList;
    }
    
    /** Get Hotels **/
    public ArrayList<HotelType> getHotels(){
        return this.hotels;
    }
    
    /** Update Time Of First Flight **/
    public void updateTimeOfFirst(int timeInMiliSeconds){
        if(timeOfFirst > timeInMiliSeconds){
            this.timeOfFirst = timeInMiliSeconds;
        }
    }
    
    /** Get Is cancel allowed **/
    public boolean getIsCancelAllowed(int now){
        boolean toReturn = true;
        if(now>timeOfFirst){
            toReturn = false;
        }   
        return toReturn;
    }
    
}
