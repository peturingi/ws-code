/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws.lameduck;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author joshweinstein
 */
public class DateHelper {
    public static boolean compareDates(XMLGregorianCalendar a, XMLGregorianCalendar b) {
        int aYear = a.getYear();
        int aMonth = a.getMonth();
        int aDay = a.getDay();
        
        int bYear = b.getYear();
        int bMonth = b.getMonth();
        int bDay = b.getDay();
        
        return aYear == bYear && aMonth == bMonth && aDay == bDay;
    }
}
