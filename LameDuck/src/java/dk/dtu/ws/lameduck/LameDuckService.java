/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws.lameduck;

import dk.dtu.imm.fastmoney.BankService;
import dk.dtu.imm.fastmoney.CreditCardFaultMessage;
import dk.dtu.imm.fastmoney.types.AccountType;
import dk.dtu.imm.fastmoney.types.CreditCardInfoType;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import types.lameduck.ws.dtu.dk.*;

/**
 * 
 * @author joshweinstein
 */
@WebService(serviceName = "LameDuckService", portName = "LameDuckPortTypeBindingPort", endpointInterface = "lameduck.ws.dtu.dk.LameDuckPortType", targetNamespace = "dk.dtu.ws.lameduck", wsdlLocation = "WEB-INF/wsdl/LameDuckService/LameDuck.wsdl")
public class LameDuckService {
//    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/fastmoney.imm.dtu.dk_8080/BankService.wsdl")
    private BankService service = new BankService();
    private static Flights flights = generateFlights();
    private AccountType account = createAccount();
    
    private static Flights generateFlights() {
        Flights allFlights = new Flights();
        for (int i = 1; i <= 10; i++) {
            String bookingNumber = String.valueOf(i);
            double price = Math.pow(10, i);
            FlightType flightType = createFlightType(1416124162000L + i*100000000, 1416124162000L + i*100000000 + 10000000);
            FlightInfoType flight = createFlightInfoType(bookingNumber, "lameduck", price, flightType);
            allFlights.getFlightInfo().add(flight);
        }
        
        for (int i = 10; i <= 20; i++) {
            String bookingNumber = String.valueOf(i);
            double price = Math.pow(10, i-10);
            FlightType flightType = createFlightType(1420224162000L + i*100000000, 1420224162000L + i*100000000 + 10000000);
            FlightInfoType flight = createFlightInfoType(bookingNumber, "lameduck", price, flightType);
            allFlights.getFlightInfo().add(flight);
        }
        
//        System.out.println(allFlights.getFlightInfo().get(0).getFlight().getDeparture());
        System.out.println("FOOOOOOOOOOOOOOOOOOOOOOOO");
        return allFlights;
    }
    
    private AccountType createAccount() {
        AccountType account = new AccountType();
        account.setName("LameDuck");
        account.setNumber("50208812");
        
        return account;
    }
    private static FlightInfoType createFlightInfoType(String bookingNumber, String bookingService, double price, FlightType flightType) {
        FlightInfoType flight = new FlightInfoType();
        flight.setBookingNumber(bookingNumber);
        flight.setFlight(flightType);
        MoneyType money = new MoneyType();
        money.setAmount(price);
        money.setCurrency("DKK");
        flight.setPrice(money);
        flight.setReservationService(bookingService);
        return flight;
    }
    private static FlightType createFlightType(long departure, long arrival) {
        FlightType flightType = new FlightType();
        // Set departure time.
        XMLGregorianCalendar departureTime = dateToXMLCal(departure);
        flightType.setDeparture(departureTime);

        // Set arrival time.
        XMLGregorianCalendar arrivalTime = dateToXMLCal(arrival);
        flightType.setArrival(arrivalTime);

        // Set carrier.
        CarrierType carrier = new CarrierType();
        carrier.setCode("LD123");
        carrier.setName("Lame Duck");
        flightType.setCarrier(carrier);
        
        // Set flight path.
        FlightPathType path = new FlightPathType();
        AirportType from = createAirportType("CPH", "Copenhagen Airport");
        path.setFrom(from);
        AirportType to = createAirportType("ATH", "Athens International Airport");
        path.setTo(to);
        flightType.setPath(path);
        
        return flightType;
    }
    private static AirportType createAirportType(String code, String name) {
        AirportType airport = new AirportType();
        airport.setCode(code);
        airport.setName(name);
        return airport;
    }
    
    private static XMLGregorianCalendar dateToXMLCal(long time) {
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(time);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return xmlCal;
    }
    
    /**
     * Provided flight query of flight path and departure date, return a Flights
     * list of flights with the departure date and flight path.
     * 
     * @param getFlightsQuery
     *  FlightsQuery containing departure date and FlightPathType of to and from locations.
     * 
     * @return 
     *  Return Flights object.
     */
    public Flights getFlights(FlightsQuery getFlightsQuery) {
        Flights queriedFlights = new Flights();
        XMLGregorianCalendar queryDeparture = getFlightsQuery.getDate();
        FlightPathType queryPath = getFlightsQuery.getPath();
        for (FlightInfoType f: flights.getFlightInfo()) {
            FlightType ft = f.getFlight();
            XMLGregorianCalendar flightDeparture = ft.getDeparture();
            FlightPathType flightPath = ft.getPath();
            
            if (Comparator.compareFlightPath(queryPath, flightPath) && DateHelper.compareDates(flightDeparture, queryDeparture)) {
                queriedFlights.getFlightInfo().add(f);
            }
        }
        return queriedFlights;
    }

    private FlightInfoType findFlight(String bookingNumber) {
        FlightInfoType flight = null;
        for (FlightInfoType f: flights.getFlightInfo()) {
            if (f.getBookingNumber().equals(bookingNumber)) {
                flight = f;
                break;
            }
        }
        return flight;
    }
    
    public boolean bookFlight(BookFlightRequest bookFlightRequest) throws lameduck.ws.dtu.dk.BookFlightFault {
        CreditCardInfoType cc = bookFlightRequest.getCreditCard();
        String bookingNumber = bookFlightRequest.getBookingNumber();
        FlightInfoType flight = findFlight(bookingNumber);
        
        if (flight == null) {
            BookFlightFault faultInfo = new BookFlightFault();
            faultInfo.setMessage("Error while booking unknown flight.");
            lameduck.ws.dtu.dk.BookFlightFault fault = new lameduck.ws.dtu.dk.BookFlightFault("Flight was null.", faultInfo);
            throw fault;
        }
        
        int price = (int)flight.getPrice().getAmount();
        
        try {
            chargeCreditCard(16, cc, price, account);
        } catch(CreditCardFaultMessage e) {
            BookFlightFault faultInfo = new BookFlightFault();
            faultInfo.setRootFault(e.getFaultInfo());
            faultInfo.setMessage("Unable to book flight with credit card information: " + cc.getNumber());
            lameduck.ws.dtu.dk.BookFlightFault fault = new lameduck.ws.dtu.dk.BookFlightFault(e.getFaultInfo().getMessage(), faultInfo);
            throw fault;
        }
        
        return true;
    }

    public void cancelFlight(CancelFlightRequest cancelFlightRequest) {
        String bookingNumber = cancelFlightRequest.getBookingNumber();
        int refundAmount = (int) cancelFlightRequest.getPrice().getAmount() / 2;
        CreditCardInfoType cc = cancelFlightRequest.getCreditCard();
        
        try {
            refundCreditCard(16, cc, refundAmount, account);
        } catch(Exception e) {
            //TODO: handle exception
        }
    }

    private boolean chargeCreditCard(int group, CreditCardInfoType creditCardInfo, int amount, AccountType account) throws CreditCardFaultMessage {
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.chargeCreditCard(group, creditCardInfo, amount, account);
    }

    private boolean refundCreditCard(int group, CreditCardInfoType creditCardInfo, int amount, AccountType account) throws CreditCardFaultMessage {
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.refundCreditCard(group, creditCardInfo, amount, account);
    }
}
